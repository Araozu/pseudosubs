import Vue from 'vue'
import Router from 'vue-router'
import Inicio from './views/Inicio.vue'
import Anime from './views/Anime.vue'
import AnimeView from './views/AnimeView.vue'

Vue.use(Router);

const rutas = {
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'Inicio',
            component: Inicio
        },
        {
            path: '/Anime/',
            name: 'AnimeIndex',
            component: Anime
        },
        {
            path: '/Anime/:anio/:temp/:anime',
            name: 'AnimeView',
            component: AnimeView
        },
        {
            path: '/a/',
            name: 'Administracion',
            component: () => import('./views/Administracion.vue')
        },
        {
            path: '/cambios',
            name: 'Lista de Cambios',
            component: () => import('./views/ListaDeCambios.vue')
        },
        {
            path: '/nosotros',
            name: 'Nosotros',
            component: () => import('./views/Nosotros.vue')
        }
    ],
    scrollBehavior (to: any, from: any, savedPosition: any) {
        // return desired position
        return { x: 0, y: 0 }
    }
};

// @ts-ignore
export default new Router(rutas);
