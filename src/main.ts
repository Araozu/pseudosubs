import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false;

require('./sass/main.sass');

(() => {
    // @ts-ignore
    window.miRouter = router;
    // @ts-ignore
    window.indiceAnimes = {};
})();



new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
