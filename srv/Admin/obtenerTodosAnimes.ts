const obtenerTodosAnimes = (req: any, res: any) => {

    const con = require('../index').conexionMySQL;
    const YAML = require('yaml');

    con.query(
        `SELECT anime_ID ,titulo FROM animes ORDER BY anime_ID DESC `,
        (err: any, respuesta: any) => {
            if (!err) {

                const resultado = YAML.stringify(respuesta);
                res.send(resultado);

            } else { // TODO

            }
        }
    );

};

module.exports.obtenerTodosAnimes = obtenerTodosAnimes;