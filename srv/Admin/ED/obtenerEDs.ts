interface RespuestaED {
    ED_ID: number,
    anime_ID: number,
    num_ED: number,
    nombre: string,
    artista: string,
    eps: string
}

const obtenerEDs = (req: any, res: any) => {

    const YAML = require('yaml');
    const anime_ID: number = req.params.anime_ID;
    const con = require('../../index').conexionMySQL;


    if (anime_ID) {

        const query = `SELECT * FROM ED WHERE anime_ID=${anime_ID}`;
        con.query(query, (err: Error, response: RespuestaED[]) => {
            if (!err) {

                res.send(YAML.stringify(response));

            } else {
                res.send("error: true");
                console.log("Error al ejecutar query en /Admin/ED/nuevoED (con.query):\n" + query + "\n" + err);
            }
        });

    } else {
        res.send("error: true");
        console.log("Error en /Admin/ED/obtenerEDs: anime_ID no existe.");
    }

};

module.exports.obtenerEDs = obtenerEDs;