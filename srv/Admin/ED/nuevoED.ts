interface Data {
    anime_ID: number,
    num_ED: number,
    nombre: string,
    artista: string,
    eps: string
}

const nuevoED = (req: any, res: any) => {

    const YAML = require('yaml');
    const data: Data = YAML.parse(req.body.data);
    const con = require('../../index').conexionMySQL;

    if (data) {

        const query = `INSERT INTO ED (anime_ID, num_ED, nombre, artista, eps) VALUES ( ${data.anime_ID}, ${data.num_ED},
          ${con.escape(data.nombre)}, ${con.escape(data.artista)}, ${con.escape(data.eps)} )`;
        con.query(query, (err: Error) => {
            if (!err) {
                res.send("exito: true");
            } else {
                res.send("error: true");
                console.log("Error al ejecutar query en /Admin/ED/nuevoED (con.query):\n" + query + "\n" + err);
            }
        });

    } else {
        res.send("error: true");
        console.log("Error. La data no existe en /Admin/ED/nuevoED (con.connect)");
    }

};

module.exports.nuevoED = nuevoED;