const crearAnime = (req: any, res: any) => {

    const con = require('../index').conexionMySQL;
    const YAML = require('yaml');

    if (req.body.data) {

        const data = YAML.parse(req.body.data);

        const query = `INSERT INTO animes 
             (titulo, link, imgUrl, descripcion, estudio, eps, alAire, temporada, anio, fuente, generos, color) 
             VALUES (${con.escape(data.titulo)}, ${con.escape(data.link)}, ${con.escape(data.imgUrl)}, ${con.escape(data.descripcion)}, 
             ${con.escape(data.estudio)}, ${data.eps}, ${con.escape(data.alAire)}, ${con.escape(data.temporada)}, ${con.escape(data.anio)},
             ${con.escape(data.fuente)}, ${con.escape(data.generos)}, ${con.escape(data.color)})`
        ;
        con.query(
            query,
            (err: any) => {
                if (!err) {
                    res.send("Exito");
                } else {
                    res.send("Fracaso :c\n" + err);
                }
            }
        );

    } else {
        res.send("Error. El servidor no recibio ningun dato.");
    }
};

module.exports.crearAnime = crearAnime;