const nuevoOP = (req: any, res: any) => {

    const con = require('../../index').conexionMySQL;
    const YAML = require('yaml');

    const data = YAML.parse(req.body.data);

    con.query(
        `INSERT INTO OP (anime_ID, num_OP, nombre, artista, eps) VALUES \
        ( ${con.escape(data['anime_ID'])}, ${con.escape(data['num_OP'])}, ${con.escape(data['nombre'])}, 
          ${con.escape(data['artista'])}, ${con.escape(data['eps'])} )`,
        (err: any) => {
            if (!err) {

                res.send("Exito :D");

            } else {

            }
        }
    );

};

module.exports.nuevoOP = nuevoOP;