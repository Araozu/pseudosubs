const obtenerOPs = (req: any, res: any) => {

    const con = require('../../index').conexionMySQL;
    const YAML = require('yaml');

    const animeID: number = req.params.id;

    if (animeID) {

        const query = `SELECT * FROM OP WHERE anime_ID=${animeID}`;
        con.query( query, (err: any, resultado: object) => {
            if (!err) {

                res.send(YAML.stringify(resultado));

            } else {
                console.log("Error al ejecutar query en /Admin/OP/obtenerOPs (con.query):\n" + query + "\n" + err);
                res.send("error: true");
            }
        });

    } else {
        console.log("Error en /Admin/OP/obtenerOPs: animeID no existe");
        res.send("error: true");
    }
};

module.exports.obtenerOPs = obtenerOPs;