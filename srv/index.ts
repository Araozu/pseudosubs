import express from 'express';
import * as core from "express-serve-static-core";
import {MysqlError} from "mysql";

export default (app: core.Express) => {

    app.use(express.json());
    app.use(express.urlencoded());

    app.get("/");

    app.post('/links', require('./LinksAnimes/obtenerLinks').obtenerLinks);
    app.put('/links', require('./LinksAnimes/modificarLink').modificarLinks);

    app.put('/a/', require('./Admin/crearAnime').crearAnime);
    app.get('/a/', require('./Admin/obtenerTodosAnimes').obtenerTodosAnimes);

    app.get('/op/:id', require('./Admin/OP/obtenerOPs').obtenerOPs);
    app.post('/op/', require('./Admin/OP/nuevoOP').nuevoOP);

    app.get('/ed/:anime_ID', require('./Admin/ED/obtenerEDs').obtenerEDs);
    app.post('/ed/', require('./Admin/ED/nuevoED').nuevoED);

    app.get('/eps/variantes/:anime_ID', require('./Admin/Eps/obtenerVariantes').obtenerVariantes);
    app.get('/eps/links/:opcion_ID', require('./Admin/Eps/obtenerLinks').obtenerLinks);
    app.post('/eps/links/', require('./Admin/Eps/crearLink').crearLink);

    app.get('/estadisticas', require('./Admin/Eps/estadisticas').estadisticas);
}

const conexionMySQL = (() => {
    const mysql = require('mysql');
    const SQL_CONNECT_DATA = require('./DatosConexion').DatosConexion;

    const con = mysql.createConnection(SQL_CONNECT_DATA);

    con.connect((err: MysqlError) => {
        if (!err) {
            console.log("MySQL iniciado c:");
        } else {
            throw err;
        }
    });

    return con;
})();

const color = '#cc2f66';

module.exports.conexionMySQL = conexionMySQL;