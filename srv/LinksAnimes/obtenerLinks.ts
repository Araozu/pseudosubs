const GestorDeTareas = require("../GestorDeTareas/GestorDeTareas").GestorDeTareas;

const YAML = require('yaml');

interface links {
    links_ID: number,
    anime_ID: number,
    aviso: string,
    sigEp: string
}
interface links_opciones {
    opcion_ID: number,
    links_ID: number,
    num_opcion: number,
    formato: string,
    res: string,
    servidor: string,
    color: string
}
interface eps {
    ep_ID: number,
    opcion_ID: number,
    num_ep: number,
    visitas: number,
    link: string,
    peso: string
}

interface LinksEps {
    ep_ID: number,
    visitas: number,
    peso: string,
    link: string
}
interface Links {
    aviso: string,
    sigEp: string
    opciones: {
        [num: number]: {
            formato: string,
            res: string,
            servidor: string,
            color: string,
            eps: {
                [num: number]: LinksEps
            }
        }
    }
}

const obtenerLinks = (req: any, res: any) => {

    const con = require('../index').conexionMySQL;
    const animeID = req.body.animeID;


    if (animeID) {

        const data: Links = {aviso: '', sigEp: '', opciones: {}};

        con.query(
            `SELECT * FROM links WHERE anime_ID=${animeID} `,
            (err: Error, response: links[]) => {
                if (!err && response[0] ) {

                    const subData: links = response[0];
                    data.aviso = subData.aviso;
                    data.sigEp = subData.sigEp;


                    const gestorOpciones = new GestorDeTareas(() => {
                        res.send(YAML.stringify(data));
                    });

                    con.query(
                        `SELECT * FROM links_opciones WHERE links_ID=${subData.links_ID}`,
                        (err: Error, response: links_opciones[]) => {
                            if (!err && response[0]) {

                                for (const opcionID in response) {
                                    const opcion = response[opcionID];

                                    gestorOpciones.agregarTarea();

                                    data.opciones[opcion.num_opcion] = {
                                        formato: opcion.formato,
                                        res: opcion.res,
                                        servidor: opcion.servidor,
                                        color: opcion.color,
                                        eps: {}
                                    };


                                    con.query(
                                        `SELECT * FROM eps WHERE opcion_ID=${opcion.opcion_ID}`,
                                        (err: Error, response: eps[]) => {
                                            if (!err && response[0]) {

                                                for (const epID in response) {
                                                    const ep = response[epID];

                                                    data.opciones[opcion.num_opcion].eps[ep.num_ep] = {
                                                        ep_ID: ep.ep_ID,
                                                        visitas: ep.visitas,
                                                        peso: ep.peso,
                                                        link: ep.link
                                                    };
                                                }

                                                gestorOpciones.terminarTarea();
                                            } else if (!err) {
                                                console.log("No existen episodios para esta variante");

                                                res.send(`{ "exito": false }`);
                                            } else {
                                                console.log("Error al obtener eps:\n" + err);
                                                res.send(`{ "exito": false }`);
                                            }
                                        }
                                    );

                                }
                            } else if (!err) {
                                console.log("No existen links_opciones ");
                                res.send(`{ "exito": false }`);
                            } else {
                                console.log("Error al obtener links opciones:\n" + err);
                                res.send(`{ "exito": false }`);
                            }
                        }
                    );

                } else if (!err) {
                    console.log("Error: La consulta no dio ningun resultado en obtenerLinks");
                    res.send('{ "exito": false }');
                } else {
                    console.log("Error al ejecutar Query en obtenerLinks.\n" + err);
                    res.send('{ "exito": false }');
                }

            }
        );


    } else {
        console.log("AnimeID no existe");
        res.send(`{ "exito": false }`);
    }

};

module.exports.obtenerLinks = obtenerLinks;