import {Connection} from "mysql";

interface RespuestaDatos {
    estudio: string,
    eps: number,
    alAire: string,
    temporada: string,
    anio: number,
    fuente: string,
    generos: string
}

interface RespuestaOP {
    OP_ID: number,
    anime_ID: number,
    num_OP: number,
    nombre: string,
    artista: string,
    eps: string
}

interface RespuestaED {
    ED_ID: number,
    anime_ID: number,
    num_ED: number,
    nombre: string,
    artista: string,
    eps: string
}

interface Respuesta extends RespuestaDatos {

}

const GestorDeTareas = require("../GestorDeTareas/GestorDeTareas").GestorDeTareas;
const YAML = require('yaml');

const obtenerDatos = (req: any, res: any) => {

    const anime_ID = req.params.anime_ID;

    const con: Connection = require('../index').conexionMySQL;

    const respuesta: any = {};
    const gestor = new GestorDeTareas(() => {
        console.log(YAML.stringify(respuesta));
        res.send(YAML.stringify(respuesta));
    });

    const query1 = `SELECT estudio, eps, alAire, temporada, anio, fuente, generos FROM animes WHERE anime_ID=${anime_ID}`;
    gestor.agregarTarea();
    con.query(query1, (err: Error, res: RespuestaDatos[]) => {
        if (!err) {

            const data = res[0];

            respuesta.estudio = data.estudio;
            respuesta.eps = data.estudio;
            respuesta.alAire = data.alAire;
            respuesta.temporada = data.temporada;
            respuesta.anio = data.anio;
            respuesta.fuente = data.fuente;
            respuesta.generos = data.generos;

            gestor.terminarTarea();

        } else {
            respuesta.error = true;
            console.log(`Error al ejecutar sentencia SQL en /Anime/obtenerDatos (con.query)(1):\n${query1}\n${err}`);
        }
    });

    const query2 = `SELECT * FROM OP WHERE anime_ID=${anime_ID}`;
    gestor.agregarTarea();
    con.query(query2, (err: Error, res: RespuestaOP[]) => {
        if (!err) {

            const ops: any = {};
            for (const op of res) {
                ops[op.num_OP] = {
                    nombre: op.nombre,
                    artista: op.artista,
                    eps: op.eps
                }
            }

            respuesta.OP = ops;
            gestor.terminarTarea();

        } else {
            respuesta.error = true;
            console.log(`Error al ejecutar sentencia SQL en /Anime/obtenerDatos (con.query)(2):\n${query2}\n${err}`);
        }
    });

    const query3 = `SELECT * FROM ED WHERE anime_ID=${anime_ID}`;
    gestor.agregarTarea();
    con.query(query3, (err: Error, res: RespuestaED[]) => {
        if (!err) {

            const eds: any = {};
            for (const ed of res) {
                eds[ed.num_ED] = {
                    nombre: ed.nombre,
                    artista: ed.artista,
                    eps: ed.eps
                }
            }

            respuesta.ED = eds;
            gestor.terminarTarea();

        } else {
            respuesta.error = true;
            console.log(`Error al ejecutar sentencia SQL en /Anime/obtenerDatos (con.query)(3):\n${query3}\n${err}`);
        }
    });

};

module.exports.obtenerDatos = obtenerDatos;