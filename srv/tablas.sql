-- Tabla Animes

CREATE TABLE animes (
  anime_ID INT AUTO_INCREMENT PRIMARY KEY,
  titulo VARCHAR(100),
  link VARCHAR(50),
  imgUrl VARCHAR(100),
  descripcion TEXT,
  estudio VARCHAR(50),
  eps INT,
  alAire VARCHAR(100),
  temporada VARCHAR(25),
  anio VARCHAR(4),
  fuente VARCHAR(20),
  generos VARCHAR(100),
  color VARCHAR(20)
);

-- Tabla que registra cuales animes estan en el menu principal

CREATE TABLE enPagPrin (
  pagPrin_ID INT AUTO_INCREMENT PRIMARY KEY,
  posicion INT,
  anime_ID INT
);

-- Tabla OP

CREATE TABLE OP (
  OP_ID INT AUTO_INCREMENT PRIMARY KEY,
  anime_ID INT,
  num_OP INT,
  nombre VARCHAR(100),
  artista VARCHAR(100),
  eps VARCHAR(50)
);

-- Tabla ED

CREATE TABLE ED (
  ED_ID INT AUTO_INCREMENT PRIMARY KEY,
  anime_ID INT,
  num_ED INT,
  nombre VARCHAR(100),
  artista VARCHAR(100),
  eps VARCHAR(50)
);

-- Tabla con los links (y sus tablas complementarias)

CREATE TABLE links (
  links_ID INT AUTO_INCREMENT PRIMARY KEY,
  anime_ID INT,
  aviso TINYTEXT,
  sigEp VARCHAR(5)
);

CREATE TABLE links_opciones (
  opcion_ID INT AUTO_INCREMENT PRIMARY KEY,
  links_ID INT,
  num_opcion INT,
  formato VARCHAR(20),
  res VARCHAR(20),
  servidor VARCHAR(20),
  color VARCHAR(20)
);

CREATE TABLE eps (
  ep_ID INT AUTO_INCREMENT PRIMARY KEY,
  opcion_ID INT,
  num_ep INT,
  visitas INT DEFAULT 0,
  link TINYTEXT,
  peso VARCHAR(10)
);